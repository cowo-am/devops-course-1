resource "aws_iam_user" "new_users" {
  count = "${length(var.usernames)}"
  name = "${element(var.usernames,count.index)}"
}

resource "aws_iam_access_key" "new_users" {
  count = length(var.usernames)
  user = element(var.usernames,count.index)
}

resource "aws_iam_account_password_policy" "strict" {
  minimum_password_length        = 8
  require_lowercase_characters   = true
  require_numbers                = true
  require_uppercase_characters   = true
  require_symbols                = true
  allow_users_to_change_password = true
}

resource "aws_iam_user_policy" "new_users" {
  count = length(var.usernames)
  name = "AllowEverything"
  user = element(var.usernames,count.index)
policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Action": "*",
    "Resource": "*"
  }
}
EOF
}

resource "aws_iam_user_login_profile" "new_users" {
  count = length(var.usernames)
  user = element(var.usernames, count.index)
  # user    = aws_iam_user.new_users.0.name
  pgp_key = "keybase:lemar555"
}