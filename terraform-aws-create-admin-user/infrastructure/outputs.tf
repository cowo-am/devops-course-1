output "user_arn" {
 value = aws_iam_user.new_users.0.arn
}

output "password" {
  value = aws_iam_user_login_profile.new_users.0.encrypted_password
}
