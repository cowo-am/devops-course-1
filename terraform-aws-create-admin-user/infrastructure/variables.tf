### main variables

variable "region" {
  type = string
  default = "us-east-1"
  description = "aws region"
}

### iam users variables

variable "usernames" {
  type = "list"
  default = ["levon_martirosyan"]
}
