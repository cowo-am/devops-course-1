const pts = require('posttoslack');

exports.handler = async (event, context) => {
    return pts.posttoslack(
        'hooks.slack.com',
        process.env.hook_url,
        'hello from lambda'
    );
};
