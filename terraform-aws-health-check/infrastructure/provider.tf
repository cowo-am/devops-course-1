# AWS provider
provider "aws" {
  shared_credentials_file = "~/.aws/credentials"
  # alias      = "virginia"
  region     = var.region
}
