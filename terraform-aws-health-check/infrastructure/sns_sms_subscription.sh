#!/usr/bin/env sh

aws sns publish --phone-number="$sms_phone_number" --message "$sms_message_body"