# 10. Create sns topic for sms notifications
resource "aws_sns_topic" "route53-healthcheck-sms" {
  name = "${var.sns_topic_name}-sms"

  # provider  = "aws.virginia"

  delivery_policy = <<EOF
{
  "http": {
    "defaultHealthyRetryPolicy": {
      "minDelayTarget": 20,
      "maxDelayTarget": 20,
      "numRetries": 3,
      "numMaxDelayRetries": 0,
      "numNoDelayRetries": 0,
      "numMinDelayRetries": 0,
      "backoffFunction": "linear"
    },
    "disableSubscriptionOverrides": false,
    "defaultThrottlePolicy": {
      "maxReceivesPerSecond": 1
    }
  }
}
EOF

  provisioner "local-exec" {
    command = "sh sns_sms_subscription.sh"
    environment = {
      sns_arn = self.arn
      sms_phone_number = var.sms_phone_number
      sms_message_body = var.sms_message_body
    }
  }
}

# Subscribe sns to sms
resource "aws_sns_topic_subscription" "sms" {
  topic_arn = aws_sns_topic.route53-healthcheck-sms.arn
  protocol  = "sms"
  endpoint  = var.sms_phone_number
}
