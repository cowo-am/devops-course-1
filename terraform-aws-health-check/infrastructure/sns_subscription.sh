#!/usr/bin/env sh

for email in $sns_emails; do
  echo $email
  aws sns subscribe --topic-arn "$sns_arn" --region=us-east-1 --protocol email --notification-endpoint "$email"
done
