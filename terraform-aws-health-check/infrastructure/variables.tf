# variable "region" {
#     type = "string"
#     default = "us-east-1"
# }

variable "ip_address" {
    default = ""
    description = "IP address to hit."
}

variable "domain_name" {
    default = ""
    description = "IP address or domain name to hit."
}

variable "resource_path" {
    default = "/"
    description = "Specific path to check."
}

variable "environment" {
    default = "dev"
}

variable "region" {
  type = string
  default = "us-east-1"
  description = "aws region"
}

variable "sns_topic_name" {
  type = string
  default = "route53-healthcheck_topic"
  description = "sns topic name"
}

variable "sns_subscription_email_address_list" {
  type = string
  default = "levon.martirosyan93@gmail.com"
  description = "List of email addresses as string(space separated)"
}

variable "slack_hook_url" {
  type = string
  default = "/services/T01KAQ27XPA/B01JM0Y44CS/V3lpNQemT3VUoXnULDhDo5Q9"
}

variable "notify_slack_lambda_function_name" {
  type = string
  default = "sns-notify-slack-about-healthcheck"
}

variable "notify_slack_lambda_version" {
  type = string
  default = "0.1-SNAPSHOT"
}

locals {
  slack_lambda_artifact = "./notify-slack/${var.notify_slack_lambda_function_name}_${var.notify_slack_lambda_version}.zip"
}

variable "sms_phone_number" {
  type = string
  default = "+37443295550"
}

variable "sms_message_body" {
  type = string
  default = "Testing Message"
}

variable "should_send_sms_notification" {
  type = bool
  default = true
}
