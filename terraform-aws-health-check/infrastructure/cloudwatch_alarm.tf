# 12. Create a cloudwatch metric alarm
resource "aws_cloudwatch_metric_alarm" "terraform_healthcheck_failed" {
  # provider = "aws.virginia"

  alarm_name          = "${var.environment}_terraform_healthcheck_failed"
  namespace           = "AWS/Route53"
  metric_name         = "HealthCheckStatus"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  period              = "60"
  statistic           = "Minimum"
  threshold           = "1"
  unit                = "None"
  dimensions = {
    HealthCheckId = "${aws_route53_health_check.terraform_healthcheck.id}"
  }
  alarm_description   = "This metric monitors whether the service endpoint is down or not."
  alarm_actions       = ["${aws_sns_topic.route53-healthcheck.arn}", "${aws_sns_topic.route53-healthcheck-sms.arn}"]
  insufficient_data_actions = ["${aws_sns_topic.route53-healthcheck.arn}", "${aws_sns_topic.route53-healthcheck-sms.arn}"]
  treat_missing_data  = "breaching"
  depends_on          = [aws_route53_health_check.terraform_healthcheck]
}
