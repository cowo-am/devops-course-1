output "sns_topic_arn" {
  value = join("", aws_sns_topic.route53-healthcheck.*.arn)
}

output "ec2_ip_address" {
  value = "${aws_instance.web-server-instance.public_ip}"
}
