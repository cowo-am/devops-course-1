# 11. Create a route53 healthcheck resource
resource "aws_route53_health_check" "terraform_healthcheck" {
  # provider = "aws.virginia"

# testing ip address that should probably be inaccessible.
#   ip_address        = "54.159.119.62"

  ip_address        = aws_instance.web-server-instance.public_ip
  # fqdn              = var.domain_name
  port              = 80
  type              = "HTTP"
  resource_path     = var.resource_path
  failure_threshold = "3"
  request_interval  = "30"
  measure_latency   = "1"
  regions           = ["eu-west-1", "us-east-1", "us-west-1"]
  cloudwatch_alarm_region = "us-east-1"
  tags = {
    Name = "${var.environment}_terraform_healthcheck"
  }
}
