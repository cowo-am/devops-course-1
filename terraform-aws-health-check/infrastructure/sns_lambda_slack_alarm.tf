# Define IAM role for lambda execution: needed before creating lambda functions
resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# Allow sns to execute created lambda function
resource "aws_lambda_permission" "sns" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.slack.function_name
  principal     = "sns.amazonaws.com"
  source_arn = aws_sns_topic.route53-healthcheck.arn
}

# Subscribe sns to lambda
resource "aws_sns_topic_subscription" "lambda" {
  topic_arn = aws_sns_topic.route53-healthcheck.arn
  protocol  = "lambda"
  endpoint  = aws_lambda_function.slack.arn
}

# Define lambda function to request slack endpoint
resource "aws_lambda_function" "slack" {
  function_name = var.notify_slack_lambda_function_name
  filename = local.slack_lambda_artifact
  source_code_hash = filebase64sha256(local.slack_lambda_artifact)
  handler = "index.handler"
  runtime = "nodejs12.x"
  role = aws_iam_role.iam_for_lambda.arn
  memory_size = 1024
  timeout = 5

  environment {
    variables = {
      hook_url = var.slack_hook_url
    }
  }
}
